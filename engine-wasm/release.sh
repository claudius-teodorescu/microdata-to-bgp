mkdir -p ../_dist/web/ ../_dist/nodejs/

# the module for browsers
wasm-pack build --release --target web
cp ./pkg/*.js ../_dist/web/
cp ./pkg/*_bg.wasm ../_dist/web/

# the module for NodeJS
wasm-pack build --release --target nodejs
cp ./pkg/*.js ../_dist/nodejs/
cp ./pkg/*_bg.wasm ../_dist/nodejs/
