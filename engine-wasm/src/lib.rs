use rdfa_template_engine_lib;
use sophia::api::prelude::*;
use sophia::inmem::graph::LightGraph;
use sophia::turtle::parser::turtle;
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub struct TemplateFiller {
    dataset_graph:
        sophia::inmem::graph::GenericLightGraph<sophia::inmem::index::SimpleTermIndex<u32>>,
}

#[wasm_bindgen]
impl TemplateFiller {
    #[wasm_bindgen(constructor)]
    pub fn new(dataset: &str) -> TemplateFiller {
        let dataset_graph: LightGraph = turtle::parse_str(dataset)
            .collect_triples()
            .expect("cannot extract triples");

        TemplateFiller { dataset_graph }
    }

    pub fn fill_template(&mut self, template: &str) -> Result<String, JsError> {
        let filled_in_template = rdfa_template_engine_lib::fill_in_template_string(
            &mut template.to_string(),
            &mut self.dataset_graph,
        );

        Ok(filled_in_template)
    }
}
