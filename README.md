# RDFa Template Engine

## Scope

This engine, developed in Rust, is designated to fill in HTML + [RDFa](https://w3.org/TR/rdfa-core/) templates with data extracted from RDF datasets in Turtle format.

## How it works

The ```HTML + RDFa``` template is processed into [basic graph patterns](https://w3.org/TR/sparql11-query/#BasicGraphPatterns), in a similar manner to the one used to extract triples from ```HTML + RDFa``` documents, see [RDFa Core 1.1 - Third Edition. Syntax and processing rules for embedding RDF through attributes](https://w3.org/TR/rdfa-core/).

The ```basic graph patterns``` are used to extract triples from the datasets associated with the template, and the triples are used to fill in the ```HTML + RDFa``` template.

This engine can be used to fill in both text content of an element, and the value of an attribute (for the syntax, see the examples below).

## Examples

### Example of a simple ```HTML + RDFa``` template

The ```HTML + RDFa``` template:

```html
<article
    xmlns="http://www.w3.org/1999/xhtml"
    prefix="dbo: http://dbpedia.org/resource/classes# skos: http://www.w3.org/2004/02/skos/core#"
    lang="en">
    <h1 about="https://kuberam.ro/">
        <span property="dbo:abbreviation">?abbreviation</span>
        <span>–</span>
        <span property="skos:prefLabel">?name</span>
    </h1>
</article>
```

The basic graph patterns that are extracted from the template:

```
<https://kuberam.ro/> <http://dbpedia.org/resource/classes#abbreviation> ?abbreviation@en .
<https://kuberam.ro/> <http://www.w3.org/2004/02/skos/core#name> ?name@en .

```

The filled in template looks as follows:

```html
<article
    xmlns="http://www.w3.org/1999/xhtml"
    prefix="dbo: http://dbpedia.org/resource/classes# skos: http://www.w3.org/2004/02/skos/core#"
    lang="en">
    <h1 about="https://kuberam.ro/">
        <span property="dbo:abbreviation">Kuberam</span>
        <span>–</span>
        <span property="skos:prefLabel">Kuberam website</span>
    </h1>
</article>
```

### Example of a complex ```HTML + RDFa``` template

The complexity is added by the presence of a list of items, each item having as contents the contents of a HTML ```x-template``` custom element (a custom element was chosed in order to avoid collisions with the HTML ```template``` element).

The ```HTML + RDFa``` template:

```html
<article
    xmlns="http://www.w3.org/1999/xhtml"
    prefix="fabio: http://purl.org/spar/fabio dcterms: http://purl.org/dc/terms/"
    lang="en"
    about="https://kuberam.ro/index.html#publications"
    property="rdf:type"
    content="rdf:Seq">
        <x-template>
            <div>
                <span property="dcterms:creator">?creator</span>
                <span>–</span>
                <span property="fabio:hasDiscipline">?discipline</span>
            </div>
        </x-template>
</article>
```

The basic graph pattern that is extracted from the template:

```
<https://kuberam.ro/index.html#publications> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> rdf:Seq .

```

The dataset to get data from is:
```
@prefix : <https://kuberam.ro/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix schema: <https://schema.org/> .
@prefix fabio: <http://purl.org/spar/fabio> .
@prefix dcterms:  <http://purl.org/dc/terms/> .

<https://kuberam.ro/index.html#publications> <rdf:type> rdf:Seq .
<https://kuberam.ro/index.html#publications> rdf:_1 :publication_1 .
<https://kuberam.ro/index.html#publications> rdf:_2 :publication_2 .
<https://kuberam.ro/index.html#publications> rdf:_3 :publication_3 .

:publication_1 dcterms:creator "Author 1"@en ;
    fabio:hasDiscipline "Digital Humanities"@en ;
    schema:url <https://kuberam.ro/publications/1> .
:publication_2 dcterms:creator "Author 2"@en ;
    fabio:hasDiscipline "Digital Humanities"@en ;
    schema:url <https://kuberam.ro/publications/2> .
:publication_3 dcterms:creator "Author 3"@en ;
    fabio:hasDiscipline "Digital Humanities"@en ;
    schema:url <https://kuberam.ro/publications/3> .

```

In this case, the HTML element that contains ```@content``` attribute with a ```rdf:Seq``` value is considered as being the container of some items that has to be rendered in HTMl by using the contents of its child ```x-template``` HTML custom element.

In order to achieve this, for every item, which is an IRI, of the sequence that is found in dataset, the whole context of the template analysis designated to generate ```basic graph patterns``` is applied to the contents of an ```x-template``` custom HTML element, with the ```@about``` attribute having as value the respective item.

The ```x-template``` custom HTML elements for a sequence will replace the ```x-template``` HTML custom element.

Thus, during the processing of the ```HTML + RDFa``` template, the ```x-template``` custom HTML element for the first item in the sequence will be as it is shown below. For the other two items of the sequence, the only difference is for the value of the ```template/@about``` attribute. When the processing is finished, the ```x-template``` elements are removed.

```html
<x-template
    xmlns="http://www.w3.org/1999/xhtml"
    prefix="fabio: http://purl.org/spar/fabio dcterms: http://purl.org/dc/terms/"
    lang="en"
    about="https://kuberam.ro/publications/1">
    <div>
        <span property="dcterms:creator">?creator</span>
        <span>–</span>
        <span property="fabio:hasDiscipline">?discipline</span>
    </div>
</x-template>
```

The basic graph pattern that is extracted from this template:

```
<https://kuberam.ro/publications/1> <http://purl.org/dc/terms/creator> ?creator .
<https://kuberam.ro/publications/1> <http://purl.org/spar/fabio/hasDiscipline> ?discipline .

```

After getting the sequence processed, the filled in ```HTML + RDFa``` template will look as follows:

```html
<article
    xmlns="http://www.w3.org/1999/xhtml"
    prefix="fabio: http://purl.org/spar/fabio dcterms: http://purl.org/dc/terms/"
    lang="en"
    about="https://kuberam.ro/index.html#publications"
    property="rdf:type"
    content="rdf:Seq">
        <div about="https://kuberam.ro/publications/1">
            <span property="dcterms:creator">Author 1</span>
            <span>–</span>
            <span property="fabio:hasDiscipline">Digital Humanities</span>
        </div>
        <div about="https://kuberam.ro/publications/2">
            <span property="dcterms:creator">Author 2</span>
            <span>–</span>
            <span property="fabio:hasDiscipline">Digital Humanities</span>
        </div>
        <div about="https://kuberam.ro/publications/3">
            <span property="dcterms:creator">Author 3</span>
            <span>–</span>
            <span property="fabio:hasDiscipline">Digital Humanities</span>
        </div>
</article>
```

### Example of a ```HTML + RDFa``` template with variable for an attribute's value

The example below considers data from the dataset given above.

The ```HTML + RDFa``` template:

```html
<article
    xmlns="http://www.w3.org/1999/xhtml"
    prefix="fabio: http://purl.org/spar/fabio dcterms: http://purl.org/dc/terms/ schema: https://schema.org/"
    lang="en"
    about="https://kuberam.ro/index.html#publications"
    property="rdf:type"
    content="rdf:Seq">
        <x-template>
            <div>
                <a href="schema:url ?url" target="_blank">
                    <span property="dcterms:creator">?creator</span>
                    <span>–</span>
                    <span property="fabio:hasDiscipline">?discipline</span>
                </a>
            </div>
        </x-template>
</article>
```

The intermediate ```HTML + RDFa``` template:

```html
<article
    xmlns="http://www.w3.org/1999/xhtml"
    prefix="fabio: http://purl.org/spar/fabio dcterms: http://purl.org/dc/terms/ schema: https://schema.org/"
    lang="en"
    about="https://kuberam.ro/index.html#publications"
    property="rdf:type"
    content="rdf:Seq">
        <x-template
            prefix="fabio: http://purl.org/spar/fabio dcterms: http://purl.org/dc/terms/ schema: https://schema.org/"
            lang="en"
            about="https://kuberam.ro/index.html#publications">
            <div>
                <a href="schema:url ?url" target="_blank">
                    <span property="dcterms:creator">?creator</span>
                    <span>–</span>
                    <span property="fabio:hasDiscipline">?discipline</span>
                    <x-attribute name="href" property="schema:url">?url</x-attribute>
                </a>
            </div>
        </x-template>
</article>
```
The processed ```HTML + RDFa``` template:

```html
<article
    xmlns="http://www.w3.org/1999/xhtml"
    prefix="fabio: http://purl.org/spar/fabio dcterms: http://purl.org/dc/terms/ schema: https://schema.org/"
    lang="en"
    about="https://kuberam.ro/index.html#publications"
    property="rdf:type"
    content="rdf:Seq">
        <div about="https://kuberam.ro/publications/1">
            <a href="https://kuberam.ro/publications/1" target="_blank">
                <span property="dcterms:creator">Author 1</span>
                <span>–</span>
                <span property="fabio:hasDiscipline">Digital Humanities</span>
            </a>
        </div>
        <div about="https://kuberam.ro/publications/2">
            <a href="https://kuberam.ro/publications/2" target="_blank">
                <span property="dcterms:creator">Author 2</span>
                <span>–</span>
                <span property="fabio:hasDiscipline">Digital Humanities</span>
            </a>
        </div>
        <div about="https://kuberam.ro/publications/3">
            <a href="https://kuberam.ro/publications/3" target="_blank">
                <span property="dcterms:creator">Author 3</span>
                <span>–</span>
                <span property="fabio:hasDiscipline">Digital Humanities</span>
            </a>
        </div>
</article>
```

## Advantages

By using RDFa templates, one can conveniently describe the contents of each page of a static website by using an ontology, and the filled in pages will contain RDFa markup that will be used by the search engines.

Also, one can use variables for attribute values, which is outside the scope of the RDFa specification.

## To do list

* First type of kub-template: @typeof="rdf:Seq" (for lists).

* Second type of kub-template: (for multiple objects for the same subject + predicate).

* Possibility of sorting the lists based upon collation for the language mentioned by @lang of the current element, which has @type = rdf:Seq.

* Pass all the attributes of kub-template to all of its children.

* Process full HTML pages.

* Possibility to remove all the RDFa attributes from the output (by using ```@kub-rdfa-attributes = true | false```, which defaults to ```true```.).

* All the elements within a ```x-template``` HTML custom element, which has ```RDFa``` attribute(s), will receive the context from the ```x-template``` HTML custom element, as needed.

* The processing of the ```basic graph patterns``` has to occur immediately after each one is extracted. Thus, in case of errors, the processing can stop immediately. Also, in case of ```x-template``` HTML custom elements, they can be processed easily, by automatically passing the context.

* For this, it is good to use the logic for processing ```RDFa``` attributes that is included in the source-code of the crate [graph-rdfa-processor](https://crates.io/crates/graph-rdfa-processor) and intercalate in it this processing.

* Meanwhile, the processing of ```x-template``` HTML custom elements is done with templates expressed as below. Note that all the context is to be passed by hand. The engine takes care of the correct values for ```@about``` attribute for each item of a sequence.

* In case of unprefixed property (property=":edition_type"), the property has to be passed to the ```http://www.w3.org/1999/xhtml/vocab``` namespace.

```html
<template
    xmlns="http://www.w3.org/1999/xhtml"
    prefix="dcterms: http://purl.org/dc/terms/ schema: https://schema.org/"
    lang="ro"
    about="https://clre.solirom.ro/home.html#linked-dictionaries">
    <div class="bibliographic-entry">
        <div class="work-title">
            <a href="schema:url ?url" target="_blank">(<span property="schema:alternateName">?siglum</span>) <span property="dcterms:title">?title</span></a>
        </div>
    </div>
</template>
```

## Resources about RDFa syntax

[RDFa in XHTML: Syntax and Processing](https://www.w3.org/TR/rdfa-syntax/)

[In RDFa, difference between property="" & rel="", and resource="" & about=""?](https://stackoverflow.com/questions/16873611/in-rdfa-difference-between-property-rel-and-resource-about)

[Digital library example. Suggested section for the RDFa Primer, Fabien Gandon, INRIA](https://www-sop.inria.fr/acacia/personnel/Fabien.Gandon/tmp/grddl/rdfaprimer/PrimerRDFaSection.html)

## RDFa templating engines

[The Callimachus Project: RDFa as a Web Template Language](https://ceur-ws.org/Vol-905/BattleEtAl_COLD2012.pdf)

[The Callimachus Project: Documentation](http://support.3roundstones.com/documentation.xhtml?view)

[The Callimachus Project: Discussion Group](https://groups.google.com/g/callimachus-discuss)

[Semargl](https://github.com/semarglproject/semargl)

[RDFa Templating](http://www.kjetil.kjernsmo.net/software/rat/)

[rdfa-template](https://github.com/awwright/rdfa-template)

[Lift RDFa Templates](https://platform.enilink.net/docs/templates/rdfa.html). Analysis: generates a SPARQL query from a HTMl+RDfa template, but uses no variables, so that the object of the result triples is stored in the text content of the corresponding HTML element.

[tempan](https://github.com/watoki/tempan). Analysis: generates a model of data in PHP, and fills the model.

[Templado](https://templado.io/start.html). Analysis: not very developed; creates a model of data in PHP, and fills the model.

[xslt-rdfa](https://github.com/doriantaylor/xslt-rdfa)

## What is below has to be included in a separated project for a static site generator

# Static site generator

## Scope

Generation of static websites, starting from templates in HTML+RDFa format, and data in RDF (Turtle) format.

## Reasons

* To have templates in native format: HTML contents in *.html file, CSS contents in *.css file, and Javascript contents in *.js file.
* To have HTML page metadata in the same format as the page itself.
* To have standardised mechanism for including parts of pages in layouts, namely [XInclude](https://w3.org/TR/xinclude-11/).

## Resources

[Shokunin, the fastest Rust-based Static Site Generator (SSG)](https://shokunin.one/)

[Font parser, shaping engine, and subsetter](https://docs.rs/allsorts/latest/allsorts/)

[Making my website faster](https://cliffle.com/blog/making-website-faster/#continue-reading)

[Optimising Web Performance: Reducing Font Loading Impact with Font Subsetting | by Matt Koevort](https://tech.loveholidays.com/optimising-web-performance-reducing-font-loading-impact-with-font-subsetting-d8444b6cddba)

[klippa, Rust binary for subsetting a font file](https://github.com/googlefonts/fontations/tree/main/klippa)

[Improving WebAssembly load times with Zero-Copy deserialization](https://wasmer.io/posts/improving-with-zero-copy-deserialization)

[https://uispin.org/ui.html](https://uispin.org/ui.html)

[https://archive.topquadrant.com/technology/sparql-rules-spin/](https://archive.topquadrant.com/technology/sparql-rules-spin/)

[https://github.com/nbittich/graph-rdfa-processor/tree/master](https://github.com/nbittich/graph-rdfa-processor/tree/master)

[https://w3.org/TR/microdata-rdf/#generate-the-triples](https://w3.org/TR/microdata-rdf/#generate-the-triples)
