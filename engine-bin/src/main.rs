use clap::{Arg, Command};
use glob::Pattern;
use rdfa_template_engine_lib;
use std::env;
use std::path::PathBuf;

fn main() {
    // get the CLI args
    let cli_matches = Command::new(env!("CARGO_PKG_NAME"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .version(env!("CARGO_PKG_VERSION"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .arg(
            Arg::new("templates_folder_path")
                .value_parser(clap::value_parser!(PathBuf))
                .index(1)
                .required(true),
        )
        .arg(Arg::new("templates_glob").index(2).required(true))
        .arg(
            Arg::new("dataset_file_path")
                .value_parser(clap::value_parser!(PathBuf))
                .index(3)
                .required(true),
        )
        .arg(
            Arg::new("result_folder_path")
                .value_parser(clap::value_parser!(PathBuf))
                .index(4)
                .required(true),
        )
        .get_matches();

    let templates_folder_path: &PathBuf = cli_matches
        .get_one("templates_folder_path")
        .expect("`templates_folder_path`is required");
    let templates_glob_arg: &String = cli_matches
        .get_one("templates_glob")
        .expect("`templates_glob`is required");
    let templates_glob: Pattern =
        Pattern::new(templates_glob_arg.as_str()).expect("invalid `templates_glob`");
    let dataset_file_path: &PathBuf = cli_matches
        .get_one("dataset_file_path")
        .expect("`dataset_file_path`is required");
    let result_folder_path: &PathBuf = cli_matches
        .get_one("result_folder_path")
        .expect("`result_folder_path`is required");

    rdfa_template_engine_lib::fill_in_files(
        templates_folder_path,
        templates_glob,
        dataset_file_path,
        result_folder_path,
    );
}
