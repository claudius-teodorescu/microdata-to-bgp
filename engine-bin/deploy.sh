cd engine-bin

metadata=$(cargo metadata --no-deps --format-version 1 | jq -r '.packages[2]')
executable_name=$(echo $metadata | jq -r '.name')
version_number=$(echo $metadata | jq -r '.version')
target="x86_64-unknown-linux-musl"

rustup target add x86_64-unknown-linux-musl
cargo build --target-dir ./target --release --target $target

curl --location --header "JOB-TOKEN: ${CI_JOB_TOKEN}" \
    --upload-file ./target/$target/release/${executable_name} \
    "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${executable_name}/$version_number/${executable_name}"
