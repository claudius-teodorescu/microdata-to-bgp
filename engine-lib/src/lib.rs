use glob::Pattern;
use graph_rdfa_processor::RdfaGraph;
use html5ever::namespace_url;
use html5ever::tendril::TendrilSink;
use html5ever::QualName;
use indexmap::IndexMap;
use kuchikiki::{traits::*, ExpandedName};
use lazy_static::lazy_static;
use regex::Regex;
use sophia::api::ns::{rdf, Namespace};
use sophia::api::prelude::*;
use sophia::api::term::{
    language_tag::LanguageTag,
    matcher::{Any, LanguageTagMatcher},
    SimpleTerm, TermKind,
};
use sophia::inmem::graph::LightGraph;
use sophia::turtle::parser::turtle;
use std::collections::{HashMap, HashSet};
use std::fs;
use std::ops::ControlFlow;
use std::path::{Path, PathBuf};

lazy_static! {
    static ref VARIABLE_REGEX: Regex = Regex::new(r#"^(rdf:Seq|\?[a-z0-9_]+[a-z0-9])$"#).unwrap();
    static ref PREDICATE_AND_VARIABLE_REGEX: Regex =
        Regex::new(r#"([a-z:]+)\s(rdf:Seq|\?[a-z_]+[a-z])(\^\^[a-z:A-Z]+)?$"#).unwrap();
    static ref X_ATTRIBUTE_REMOVE_REGEX: Regex =
        Regex::new(r#"<kub-attribute.+<\/kub-attribute>"#).unwrap();
    static ref X_TEMPLATE_REMOVE_REGEX: Regex =
        Regex::new(r#"(?ms)(<kub-template .*?>)(.*?)(<\/kub-template>)$"#).unwrap();
}

const RDF_SEQ: &str = "rdf:Seq";
const KUB_TEMPLATE_LOCAL_NAME: &str = "kub-template";
const KUB_PROCESSED_TEMPLATE_LOCAL_NAME: &str = "x-processed-template";
const KUB_ATTRIBUTE_LOCAL_NAME: &str = "kub-attribute";

pub fn fill_in_files(
    templates_folder_path: &Path,
    templates_glob: Pattern,
    dataset_file_path: &Path,
    result_folder_path: &Path,
) {
    let templates_path = templates_folder_path.join(templates_glob.as_str());

    for template_file_path in glob::glob(templates_path.to_str().unwrap())
        .expect("Failed to read glob pattern")
        .filter_map(Result::ok)
    {
        fill_in_file(
            templates_folder_path,
            template_file_path,
            dataset_file_path,
            result_folder_path,
        );
    }
}

fn fill_in_file(
    templates_folder_path: &Path,
    template_file_path: PathBuf,
    dataset_file_path: &Path,
    result_folder_path: &Path,
) {
    // generate the relative file path
    let template_relative_path = get_relative_path(templates_folder_path, &template_file_path);

    let mut template_file_contents =
        fs::read_to_string(template_file_path).expect("cannot read file contents");

    let dataset_file_contents =
        fs::read_to_string(dataset_file_path).expect("cannot read file contents");
    let mut dataset_graph: LightGraph = turtle::parse_str(dataset_file_contents.as_str())
        .collect_triples()
        .expect("cannot extract triples");

    template_file_contents =
        fill_in_template_string(&mut template_file_contents, &mut dataset_graph);

    fs::write(
        result_folder_path.join(template_relative_path),
        &template_file_contents,
    )
    .expect("Write file.");
}

pub fn fill_in_template_string(
    template: &mut String,
    dataset_graph: &mut sophia::inmem::graph::GenericLightGraph<
        sophia::inmem::index::SimpleTermIndex<u32>,
    >,
) -> String {
    // pre-process the HTML + RDFa template
    *template = preprocess_xhtml_rdf_template(template.as_str(), dataset_graph);

    let basic_graph_patterns_graph: LightGraph =
        xhtml_rdf_to_basic_graph_patterns(template.as_str());

    let variable_bindings =
        basic_graph_patterns_to_triples(basic_graph_patterns_graph, dataset_graph);

    // fill in the template
    for (variable, binding) in variable_bindings.into_iter() {
        *template = template.replace(variable.as_str(), binding.as_str());
    }

    *template = postprocess_xhtml_rdfa_template(template.as_str());

    template.to_string()
}

fn xhtml_rdf_to_basic_graph_patterns(template_file_contents: &str) -> LightGraph {
    let basic_graph_patterns = RdfaGraph::parse_str(template_file_contents, "", None).unwrap();

    // filtering of the variables, to have them uniques and in `?variable_name` format
    // all these checks have to be done in a special xhtml+rdf to basic graph patterns module or library
    let mut filtered_variables: HashSet<String> = HashSet::new();
    let basic_graph_patterns_graph: LightGraph = turtle::parse_str(basic_graph_patterns.as_str())
        .collect_triples()
        .expect("cannot extract triples");
    basic_graph_patterns_graph
        .triples()
        .for_each_triple(|basic_graph_pattern| {
            let variable_term = basic_graph_pattern[2];
            let mut variable = "".to_string();

            match variable_term {
                sophia::api::term::SimpleTerm::LiteralDatatype(value, _) => {
                    variable = value.as_ref().to_string();
                }
                sophia::api::term::SimpleTerm::LiteralLanguage(value, _) => {
                    variable = value.as_ref().to_string();
                }
                _ => (),
            };

            // filter out the empty and illegal variables
            let variable_without_quotes = variable.replace("\"", "");

            if variable_without_quotes != "" {
                if !VARIABLE_REGEX.is_match(variable_without_quotes.as_str()) {
                    panic!(
                        "The variable `{}` of the basic graph pattern {:?} is not well formated. \
                The legal format is `?variable_name`.",
                        variable_without_quotes, basic_graph_pattern
                    );
                } else {
                    let variable_is_duplicated = !filtered_variables.insert(variable);

                    if variable_is_duplicated {
                        panic!(
                            "The variable `{}` of the basic graph pattern {:?} is duplicated.",
                            variable_without_quotes, basic_graph_pattern
                        );
                    }
                }
            }
        })
        .expect("cannot get triple");

    basic_graph_patterns_graph
}

fn basic_graph_patterns_to_triples(
    basic_graph_patterns_graph: LightGraph,
    dataset_graph: &mut LightGraph,
) -> HashMap<String, String> {
    let mut variable_bindings: HashMap<String, String> = HashMap::new();

    basic_graph_patterns_graph.triples().for_each_triple(|basic_graph_pattern| {
        let subject_term = basic_graph_pattern[0];
        let predicate_term = basic_graph_pattern[1];
        let variable_term = basic_graph_pattern[2];
        let variable_lexical_form = variable_term.lexical_form().unwrap();
        let mut variable = "".to_string();

        let mut dataset_property: Option<String> = None;

        // cases when the object is not `rdf:Seq`
        if variable_lexical_form != RDF_SEQ {
            match variable_term {
                // the case when the basic graph pattern's variable is datatype string
                sophia::api::term::SimpleTerm::LiteralDatatype(value, _) => {
                        variable = value.as_ref().to_string();
                        let triples = dataset_graph.triples_matching(Some(subject_term), Some(predicate_term), Any);

                    for triple in triples.into_iter() {
                        let triple = triple.unwrap();

                        let object_term = triple[2];

                        match object_term {
                            // the case when at least one property is datatype string
                            SimpleTerm::LiteralDatatype(value, _) => {
                                dataset_property = Some(value.as_ref().to_string());
                                break;
                            }
                            SimpleTerm::Iri(iri) => {
                                dataset_property = Some(iri.as_str().to_string());
                                break;
                            }
                            // the case when at least one property is language string
                            SimpleTerm::LiteralLanguage(_, _) => {
                                break;
                            }
                            _ => (),
                        }

                    }

                    if dataset_property.is_none() {
                        panic!("The variable of the basic graph pattern {:?} has no language tag, \
                        and at least one triple in dataset has language tag. So, you need to set a \
                        language tag in the template.", basic_graph_pattern);
                    }
                },
                // the case when the basic graph pattern's variable is language-tagged string
                sophia::api::term::SimpleTerm::LiteralLanguage(value, tag) => {
                    variable = value.as_ref().to_string();
                    let basic_graph_pattern_language_tag = LanguageTag::new_unchecked(tag.as_str());
                    let triples = dataset_graph.triples_matching(Some(subject_term), Some(predicate_term), LanguageTagMatcher::new(basic_graph_pattern_language_tag));
                    for triple in triples.into_iter() {
                        let triple = triple.unwrap();

                        let object_term = triple[2];
                        match object_term {
                            // the case when at least one property is language string
                            SimpleTerm::LiteralLanguage(value, dataset_language_tag) => {
                                if dataset_language_tag.as_ref() == basic_graph_pattern_language_tag {
                                    dataset_property = Some(value.as_ref().to_string());
                                    break;
                                }
                            }
                            _ => (),
                        }
                    }

                    if dataset_property.is_none() {
                        panic!("The variable of the basic graph pattern {:?} has a language tag, `{:?}`, \
                        that is different to all the language tags in the dataset.", basic_graph_pattern, basic_graph_pattern_language_tag);
                    }
                },
                _ => ()
            };

            variable_bindings.insert(variable, dataset_property.unwrap());
        }
    }).expect("cannot get triple");

    variable_bindings
}

fn get_relative_path(folder_path: &Path, file_path: &PathBuf) -> String {
    let mut text_file_relative_path_buffer = PathBuf::new();
    file_path
        .components()
        .skip(folder_path.components().collect::<Vec<_>>().len())
        .for_each(|component| text_file_relative_path_buffer.push(component));
    let text_file_relative_path = text_file_relative_path_buffer.to_str().unwrap();

    text_file_relative_path.to_owned()
}

fn preprocess_xhtml_rdf_template(template: &str, dataset_graph: &mut LightGraph) -> String {
    let document = kuchikiki::parse_html().one(template);

    let template_elements_with_about_attr = document
        .select(format!("{}[about]", KUB_TEMPLATE_LOCAL_NAME).as_str())
        .unwrap()
        .collect::<Vec<_>>();

    for template_element_with_about_attr in template_elements_with_about_attr {
        let ancestor_about_attribute_value = "".to_string();

        process_x_template_element(
            template_element_with_about_attr,
            ancestor_about_attribute_value,
            dataset_graph,
        );
    }

    let template_elements_without_about_attr = document
        .select(format!("{}:not([about])", KUB_TEMPLATE_LOCAL_NAME).as_str())
        .unwrap()
        .collect::<Vec<_>>();
    for template_element_without_about_attr in template_elements_without_about_attr {
        // loop over the ancestors, in order to get the value of @about for the first kub-template ancestor node
        let ancestor_elements = template_element_without_about_attr
            .as_node()
            .ancestors()
            .elements()
            .collect::<Vec<_>>();
        for ancestor_element in ancestor_elements {
            if ancestor_element.name.local.to_string() == KUB_PROCESSED_TEMPLATE_LOCAL_NAME {
                let ancestor_element_attributes =
                    &mut ancestor_element.attributes.clone().into_inner();
                let ancestor_about_attribute_value = ancestor_element_attributes
                    .get("resource")
                    .unwrap()
                    .to_string();

                process_x_template_element(
                    template_element_without_about_attr.clone(),
                    ancestor_about_attribute_value,
                    dataset_graph,
                );
            }
        }
    }

    // Process the attributes having values in form of `predicate + variable`,
    // as well as the @resource attributes.
    // Also, add index to variables from templates, in order to avoid duplicated variable names.
    // The attribute is converted to a custom element, as follows:
    // @attribute-name="attribute-value" to
    // <kub-attribute name="attribute-name" value="attribute-value" />

    // prepare the kub-template element
    let all_elements = document.select("*").unwrap().collect::<Vec<_>>();
    for (index, any_element) in all_elements.iter().enumerate() {
        // process the attributes having a ```predicate + object``` as value
        let element_attributes = &mut any_element.attributes.clone().into_inner();
        for (attribute_expanded_name, attribute) in element_attributes.clone().map.into_iter() {
            // N.B.: No prefix is considered, as kuchikiki does not seem to consider prefixed attribute names
            let attribute_local_name = attribute_expanded_name.local.to_string();
            let attribute_value = attribute.value;

            if PREDICATE_AND_VARIABLE_REGEX.is_match(attribute_value.as_str()) {
                let (property, mut variable) = attribute_value.split_once(" ").unwrap();
                let mut variable_datatype = "";
                if let Some((value, datatype)) = variable.split_once("^^") {
                    variable = value;
                    variable_datatype = datatype;
                };

                append_x_attribute_element(
                    attribute_local_name.clone(),
                    property,
                    variable,
                    variable_datatype,
                    index,
                    any_element,
                );
            }

            if VARIABLE_REGEX.is_match(attribute_value.as_str()) {
                let property = "";
                let variable = attribute_value.as_str();
                let variable_datatype = "";

                append_x_attribute_element(
                    attribute_local_name.clone(),
                    property,
                    variable,
                    variable_datatype,
                    index,
                    any_element,
                );
            }
        }

        // Process the text content of the element, in case it is a ```variable```,
        // except the ```kub-attribute``` element
        let text_content = any_element.text_contents();
        let any_element_local_name = &any_element.name.local.to_string();

        if any_element_local_name.as_str() != KUB_PROCESSED_TEMPLATE_LOCAL_NAME
            && VARIABLE_REGEX.is_match(text_content.as_str())
        {
            // delete all the child text nodes
            let descendant_text_nodes = any_element.as_node().descendants();
            for descendant_text_node in descendant_text_nodes {
                descendant_text_node.detach();
            }

            // append the new text content to the element
            let new_text_content =
                kuchikiki::NodeRef::new_text(format!("{}_{}", text_content, index));
            any_element.as_node().append(new_text_content);
        }
    }

    let result_element = document.select_first("body > *").unwrap();
    let mut result_bytes = Vec::new();
    result_element
        .as_node()
        .serialize(&mut result_bytes)
        .unwrap();
    let result_string = String::from_utf8(result_bytes).unwrap();
    //println!("{}", result_string);
    result_string
}

fn append_x_attribute_element(
    attribute_local_name: String,
    property: &str,
    mut variable: &str,
    variable_datatype: &str,
    index: usize,
    element: &kuchikiki::NodeDataRef<kuchikiki::ElementData>,
) {
    let mut x_attribute_attributes: IndexMap<ExpandedName, kuchikiki::Attribute> = IndexMap::new();

    // add @name="attribute-name"
    x_attribute_attributes.insert(
        kuchikiki::ExpandedName::new(html5ever::ns!(), html5ever::LocalName::from("name")),
        kuchikiki::Attribute {
            prefix: None,
            value: attribute_local_name,
        },
    );

    // add @value="attribute-value"
    x_attribute_attributes.insert(
        kuchikiki::ExpandedName::new(html5ever::ns!(), html5ever::LocalName::from("property")),
        kuchikiki::Attribute {
            prefix: None,
            value: property.to_string(),
        },
    );

    // add @variable="variable-value"
    x_attribute_attributes.insert(
        kuchikiki::ExpandedName::new(html5ever::ns!(), html5ever::LocalName::from("variable")),
        kuchikiki::Attribute {
            prefix: None,
            value: variable.to_string(),
        },
    );

    // add @datatype="variable_datatype"
    if variable_datatype != "" {
        x_attribute_attributes.insert(
            kuchikiki::ExpandedName::new(html5ever::ns!(), html5ever::LocalName::from("datatype")),
            kuchikiki::Attribute {
                prefix: None,
                value: variable_datatype.to_string(),
            },
        );

        variable = variable;
    }

    // generate the kub-attribute element
    let x_attribute_qual_name = html5ever::QualName::new(
        None,
        html5ever::ns!(html),
        html5ever::LocalName::from(KUB_ATTRIBUTE_LOCAL_NAME),
    );
    let x_attribute_element =
        kuchikiki::NodeRef::new_element(x_attribute_qual_name.clone(), x_attribute_attributes);

    // add the variable
    let x_attribute_text_content = kuchikiki::NodeRef::new_text(format!("{}_{}", variable, index));
    x_attribute_element.append(x_attribute_text_content);

    // append the kub-attribute element to the current element
    element.as_node().append(x_attribute_element);
}

fn process_x_template_element(
    template_element: kuchikiki::NodeDataRef<kuchikiki::ElementData>,
    ancestor_about_attribute_value: String,
    dataset_graph: &mut sophia::inmem::graph::GenericLightGraph<
        sophia::inmem::index::SimpleTermIndex<u32>,
    >,
) {
    let template_node = template_element.as_node();

    let mut template_contents_bytes = Vec::new();
    template_node
        .serialize(&mut template_contents_bytes)
        .unwrap();
    let template_html = String::from_utf8(template_contents_bytes).unwrap();
    let template_element_inner_html = X_TEMPLATE_REMOVE_REGEX
        .replace(template_html.as_str(), "$2")
        .into_owned();

    // A workaround to get the basic graph pattern for template with all the IRI terms as absolute IRIs;
    // this has to be replaced with a proper way to determine the context form each HTML element, during traversal.
    let template_element_outer_html = format!(
        "{}</{}>",
        template_html
            .get(0..template_html.find(">").unwrap() + 1)
            .unwrap(),
        KUB_TEMPLATE_LOCAL_NAME
    );
    let template_basic_graph_pattern =
        RdfaGraph::parse_str(template_element_outer_html.as_str(), "", None).unwrap();

    // get the terms of the basic graph pattern for template
    let basic_grap_pattern_terms: Vec<&str> = template_basic_graph_pattern.split(' ').collect();
    let mut subject_for_template_basic_graph_pattern = basic_grap_pattern_terms[0]
        .replace("<", "")
        .replace(">", "");
    if subject_for_template_basic_graph_pattern == "/" {
        subject_for_template_basic_graph_pattern = ancestor_about_attribute_value;
    }
    let predicate_for_template_basic_graph_pattern = basic_grap_pattern_terms[1]
        .replace("<", "")
        .replace(">", "");

    let about_iri_namespace = Namespace::new(subject_for_template_basic_graph_pattern).unwrap();
    let about_iri_term = about_iri_namespace.iri();
    let property_iri_namespace =
        Namespace::new(predicate_for_template_basic_graph_pattern).unwrap();
    let property_iri_term = property_iri_namespace.iri();

    process_template_basic_graph_patterns(
        dataset_graph,
        about_iri_term,
        property_iri_term,
        template_element_inner_html,
        &template_element,
    );

    // remove the `template` element
    template_element.as_node().detach();
}

fn process_template_basic_graph_patterns(
    dataset_graph: &mut sophia::inmem::graph::GenericLightGraph<
        sophia::inmem::index::SimpleTermIndex<u32>,
    >,
    about_iri_term: Option<sophia::api::prelude::IriRef<sophia::api::MownStr>>,
    property_iri_term: Option<sophia::api::prelude::IriRef<sophia::api::MownStr>>,
    template_element_inner_html: String,
    template_element: &kuchikiki::NodeDataRef<kuchikiki::ElementData>,
) {
    // prepare the kub-template element
    let x_processed_template_qual_name = html5ever::QualName::new(
        None,
        html5ever::ns!(html),
        html5ever::LocalName::from(KUB_PROCESSED_TEMPLATE_LOCAL_NAME),
    );

    if property_iri_term.clone().unwrap().to_string()
        == "http://www.w3.org/1999/02/22-rdf-syntax-ns#type"
    {
        let triples = dataset_graph
            .triples_matching(about_iri_term.clone(), Any, Any)
            .collect::<Vec<_>>();

        for triple in triples.into_iter() {
            if let ControlFlow::Break(_) = expand_template_element(
                triple,
                &x_processed_template_qual_name,
                &template_element_inner_html,
                template_element,
            ) {
                continue;
            }
        }
    } else {
        let triples = dataset_graph.triples_matching(about_iri_term, property_iri_term, Any);

        for triple in triples.into_iter() {
            if let ControlFlow::Break(_) = expand_template_element(
                triple,
                &x_processed_template_qual_name,
                &template_element_inner_html,
                template_element,
            ) {
                continue;
            }
        }
    }
}

fn expand_template_element(
    triple: Result<[&SimpleTerm<'_>; 3], sophia::inmem::index::TermIndexFullError>,
    x_processed_template_qual_name: &QualName,
    template_element_inner_html: &String,
    template_element: &kuchikiki::NodeDataRef<kuchikiki::ElementData>,
) -> ControlFlow<()> {
    let triple = triple.unwrap();
    let predicate_term = triple[1];

    if predicate_term == &rdf::type_.as_simple() {
        return ControlFlow::Break(());
    } else {
        let object_term = triple[2];
        let object_value = match object_term.kind() {
            TermKind::Iri => {
                let object_iri = object_term.iri().unwrap();
                let object_iri_ref = object_iri.as_str();

                object_iri_ref.to_string()
            }
            TermKind::Literal => {
                let object_lexical_form = object_term.lexical_form().unwrap();

                object_lexical_form.to_string()
            }
            _ => "".to_string(),
        };

        // fill the fragment attributes
        let template_attributes = &template_element.attributes.clone().into_inner();
        let mut processed_template_attributes: IndexMap<ExpandedName, kuchikiki::Attribute> =
            IndexMap::new();

        for (template_attribute_name, template_attribute_value) in template_attributes.map.clone() {
            //let fragment_attribute_prefix = template_attribute_value.prefix.unwrap().to_string();
            let mut fragment_attribute_local_name = template_attribute_name.local.to_string();
            let mut fragment_attribute_value = template_attribute_value.value;

            // remove @typeof and @property
            if fragment_attribute_local_name == "typeof"
                || fragment_attribute_local_name == "property"
            {
                continue;
            }

            // process @about
            if fragment_attribute_local_name == "about" {
                fragment_attribute_local_name = "resource".to_string();
                fragment_attribute_value = object_value.clone();
            }

            //process @resource
            if fragment_attribute_local_name == "resource" {
                fragment_attribute_value = object_value.clone();
            }

            // generate and append the new attribute
            let fragment_attribute_expanded_name = kuchikiki::ExpandedName::new(
                html5ever::ns!(),
                html5ever::LocalName::from(fragment_attribute_local_name),
            );
            let fragment_attribute = kuchikiki::Attribute {
                prefix: None,
                value: fragment_attribute_value,
            };
            processed_template_attributes
                .insert(fragment_attribute_expanded_name, fragment_attribute);
        }

        // generate the kub-template element
        let x_template_element = kuchikiki::NodeRef::new_element(
            x_processed_template_qual_name.clone(),
            processed_template_attributes.clone(),
        );

        // append the HTML template's contents to the kub-template element
        let fragment_attributes: Vec<html5ever::Attribute> = Vec::new();
        let fragment_element =
            kuchikiki::parse_fragment(x_processed_template_qual_name.clone(), fragment_attributes)
                .one(template_element_inner_html.clone());
        let processed_template_element = fragment_element.select_first("html > *").unwrap();

        x_template_element.append(processed_template_element.as_node().clone());

        // append the kub-template element to the document
        template_element
            .as_node()
            .insert_before(x_template_element.clone());
    }
    ControlFlow::Continue(())
}

// Removes the kub-template and kub-attribute elements, along with some postprocessing
fn postprocess_xhtml_rdfa_template(template: &str) -> String {
    let document = kuchikiki::parse_html().one(template);

    // prepare the x-processed-template element
    let x_processed_template_qual_name = html5ever::QualName::new(
        None,
        html5ever::ns!(html),
        html5ever::LocalName::from(KUB_PROCESSED_TEMPLATE_LOCAL_NAME),
    );

    // process the kub-attribute elements, by using a workaround (string replacement),
    // as I had not found a way to update an attribute in kuchikiki
    let all_elements = document.select("*").unwrap().collect::<Vec<_>>();

    // loop over all the elements, in order to process only the ones that have kub-attribute child nodes
    for current_element in all_elements {
        let current_node = current_element.as_node();
        let mut parent_node_outer_html = current_node.to_string();
        let mut x_attribute_elements_exist = false;

        for child_node in current_node.children().collect::<Vec<_>>() {
            // this is an element
            if let Some(child_element) = child_node.as_element() {
                let child_element_local_name = child_element.name.local.to_string();

                // this is a kub-attribute element
                if child_element_local_name == KUB_ATTRIBUTE_LOCAL_NAME {
                    x_attribute_elements_exist = true;
                    // get the value of @property, the element's text contents, and the value of @datatype
                    let attributes = child_element.attributes.clone().into_inner();
                    let property = attributes.get("property").unwrap();
                    let datatype_attribute = attributes.get("datatype");
                    let variable = attributes.get("variable").unwrap();
                    let variable_value = child_node.text_contents();
                    let mut variable_binding = format!("{} {}", property, variable);

                    if let Some(datatype) = datatype_attribute {
                        variable_binding = format!("{} {}^^{}", property, variable, datatype);
                    };

                    parent_node_outer_html = parent_node_outer_html
                        .replace(variable_binding.as_str(), variable_value.as_str());
                }
            }
        }

        if x_attribute_elements_exist {
            // create a document fragment having as content the parent node's outer HTML
            let fragment_attributes: Vec<html5ever::Attribute> = Vec::new();
            let fragment_inner_html =
                X_ATTRIBUTE_REMOVE_REGEX.replace(parent_node_outer_html.as_str(), "");
            let fragment_element = kuchikiki::parse_fragment(
                x_processed_template_qual_name.clone(),
                fragment_attributes,
            )
            .one(fragment_inner_html.into_owned());

            // replace the parent node with the document fragment
            let new_parent_element = fragment_element.select_first("html > *").unwrap();
            let new_parent_node = new_parent_element.as_node();
            current_node.insert_after(new_parent_node.clone());
            current_node.detach();
        }
    }

    let result_element = document.select_first("body > *").unwrap();
    let mut result_bytes = Vec::new();
    result_element
        .as_node()
        .serialize(&mut result_bytes)
        .unwrap();
    let mut result_string = String::from_utf8(result_bytes).unwrap();
    // replace the kub-template elements with div elements
    result_string = result_string.replace(KUB_PROCESSED_TEMPLATE_LOCAL_NAME, "div");

    result_string
}

#[test]
pub fn test_1() {
    let templates_folder_path = Path::new("/home/claudius/workspace/repositories/git/gitlab.com/solirom-clre/sites/site/public/content/");
    let templates_glob = Pattern::new("**/home.html").expect("invalid `templates_glob`");
    let dataset_file_path = Path::new("/home/claudius/workspace/repositories/git/gitlab.com/solirom-clre/sites/site/_build/data/global-ontology.ttl");
    let result_folder_path = Path::new("/home/claudius/workspace/repositories/git/gitlab.com/solirom-clre/sites/site/public/content/");

    fill_in_files(
        templates_folder_path,
        templates_glob,
        dataset_file_path,
        result_folder_path,
    );
}

#[test]
pub fn test_2() {
    let html = r#"
        <kub-template xmlns="http://www.w3.org/1999/xhtml"
            prefix="schema: https://schema.org/ skos: http://www.w3.org/2004/02/skos/core#"
            lang="ro" resource="https://lingv.ro/">
            <a href="?creator_url" target="_blank" property="schema:url">?creator_name</a>
        </kub-template>
    "#;

    let basic_graph_patterns = RdfaGraph::parse_str(html, "", None).unwrap();

    println!("{}", basic_graph_patterns);
}

/*
    // get the depth of the tree
    let mut level_index = 1;
    let mut current_level_all_elements_number = document
        .select("html > body > * > *")
        .unwrap()
        .collect::<Vec<_>>()
        .len();

    while current_level_all_elements_number > 0 {
        let css_selector_fragment = std::iter::repeat(" > *")
            .take(level_index)
            .collect::<String>();

        current_level_all_elements_number = document
            .select(format!("html > body > *{}", css_selector_fragment).as_str())
            .unwrap()
            .collect::<Vec<_>>()
            .len();

        level_index += 1;
    }
*/
